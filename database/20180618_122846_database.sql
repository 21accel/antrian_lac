-- ---------------------------------------------------------
--
-- SIMPLE SQL Dump
-- 
-- nawa (at) yahoo (dot) com
--
-- Host Connection Info: localhost via TCP/IP
-- Generation Time: June 18, 2018 at 12:28 PM ( ASIA/JAKARTA )
-- PHP Version: 5.5.38
--
-- ---------------------------------------------------------



SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


-- ---------------------------------------------------------
--
-- Table structure for table : `antrian`
--
-- ---------------------------------------------------------

CREATE TABLE `antrian` (
  `ID` bigint(20) NOT NULL,
  `tanggal` date NOT NULL,
  `loket` varchar(7) DEFAULT NULL,
  `no_antrian` char(3) NOT NULL,
  `panggil` char(5) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '0',
  `updated_user` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `antrian`
--

INSERT INTO `antrian` (`ID`, `tanggal`, `loket`, `no_antrian`, `panggil`, `status`, `updated_user`, `updated_date`) VALUES
(1, '2018-06-18', 'Loket 1', 001, '0 0 1', 1, 2, '2018-06-18 11:24:38'),
(2, '2018-06-18', 'Loket 2', 002, '0 0 2', 1, 3, '2018-06-18 11:29:27'),
(3, '2018-06-18', 'Loket 1', 003, '0 0 3', 1, 2, '2018-06-18 12:22:06'),
(4, '2018-06-18', 'Loket 3', 004, '0 0 4', 1, 4, '2018-06-18 12:22:21'),
(5, '2018-06-18', 'Loket 4', 005, '0 0 5', 1, 5, '2018-06-18 12:22:41'),
(6, '2018-06-18', '', 006, '0 0 6', 0, '', ''),
(7, '2018-06-18', '', 007, '0 0 7', 0, '', ''),
(8, '2018-06-18', '', 008, '0 0 8', 0, '', '');



-- ---------------------------------------------------------
--
-- Table structure for table : `sys_config`
--
-- ---------------------------------------------------------

CREATE TABLE `sys_config` (
  `configID` tinyint(1) NOT NULL,
  `nama_instansi` varchar(100) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `telepon` varchar(13) NOT NULL,
  `email` varchar(50) NOT NULL,
  `website` varchar(50) NOT NULL,
  `logo` varchar(50) NOT NULL,
  `updated_user` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`configID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sys_config`
--

INSERT INTO `sys_config` (`configID`, `nama_instansi`, `alamat`, `telepon`, `email`, `website`, `logo`, `updated_user`, `updated_date`) VALUES
(1, 'Nusantara', 'Jalan Radin Inten No. 100, Bandar Lampung, Lampung', 081377783334, 'indra.setyawantoro@gmail.com', 'www.indrasatya.com', 'logo.png', 1, '2018-06-14 10:20:58');



-- ---------------------------------------------------------
--
-- Table structure for table : `sys_database`
--
-- ---------------------------------------------------------

CREATE TABLE `sys_database` (
  `dbID` int(11) NOT NULL,
  `file_name` varchar(50) NOT NULL,
  `file_size` varchar(10) NOT NULL,
  `created_user` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`dbID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



-- ---------------------------------------------------------
--
-- Table structure for table : `sys_users`
--
-- ---------------------------------------------------------

CREATE TABLE `sys_users` (
  `userID` int(11) NOT NULL,
  `fullname` varchar(30) NOT NULL,
  `user_account_name` varchar(30) NOT NULL,
  `user_account_password` varchar(45) NOT NULL,
  `user_permissions` enum('Administrator','Loket 1','Loket 2','Loket 3','Loket 4') NOT NULL,
  `block_users` enum('Ya','Tidak') NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_user` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `sys_users`
--

INSERT INTO `sys_users` (`userID`, `fullname`, `user_account_name`, `user_account_password`, `user_permissions`, `block_users`, `last_login`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
(1, 'Indra Styawantoro', 'admin', '90b9aa7e25f80cf4f64e990b78a9fc5ebd6cecad', 'Administrator', 'Tidak', '2018-06-18 12:28:12', 1, '2018-06-13 00:00:00', 1, '2018-06-18 12:28:12'),
(2, 'Danang Kesuma', 'loket1', '8cec719e846091925976f10fe19891310fee57db', 'Loket 1', 'Tidak', '2018-06-18 12:21:47', 1, '2018-06-15 23:22:54', 2, '2018-06-18 12:21:47'),
(3, 'Kadina Putri', 'loket2', 'e0748e097924471fcad9f5056967f07c5f24c9bc', 'Loket 2', 'Tidak', '2018-06-18 11:29:17', 1, '2018-06-15 23:23:26', 3, '2018-06-18 11:29:17'),
(4, 'Dedi Saputra', 'loket3', 'f6568553ac13f53110e224dcfd8b20e351ac178e', 'Loket 3', 'Tidak', '2018-06-18 12:22:17', 1, '2018-06-18 12:19:52', 4, '2018-06-18 12:22:17'),
(5, 'Rinaldi', 'loket4', '1c23d771732306f3443713da1b724ac498995feb', 'Loket 4', 'Tidak', '2018-06-18 12:22:33', 1, '2018-06-18 12:20:32', 5, '2018-06-18 12:22:33');


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;