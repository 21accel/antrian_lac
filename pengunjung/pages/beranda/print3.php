<?php
//date_default_timezone_set("ASIA/JAKARTA");

// panggil file config.php untuk koneksi ke database
require_once "../../../config/config.php";
// panggil file fungsi nama hari
require_once "../../../config/fungsi_nama_hari.php";

require 'pengunjung/vendor/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;


$hari_ini = date("Y-m-d");

$configID = "1";
// fungsi query untuk menampilkan data dari tabel sys_config
$result = $mysqli->query("SELECT nama_instansi FROM sys_config WHERE configID='$configID'") or die('Ada kesalahan pada query tampil data config: '.$mysqli->error);
$data_config = $result->fetch_assoc();

// fungsi query untuk menampilkan data dari tabel antrian
$result = $mysqli->query("SELECT max(no_antrian) as nomor, loket FROM antrian WHERE tanggal='$hari_ini' ORDER BY no_antrian DESC LIMIT 1") or die('Ada kesalahan pada query tampil nomor antrian: '.$mysqli->error);
$data = $result->fetch_assoc();

$nama_instansi  = $data_config['nama_instansi'];
$loket          = $data['loket'];
$no_antrian     = $data['nomor'];
$hari           = date("l");
$tanggal        = date("d-m-Y");
$jam            = date("H:i:s");

$connector = new WindowsPrintConnector("POS-58");
$printer = new Printer($connector);
if($printer)
{
    $printer->setJustification(Printer::JUSTIFY_CENTER);

    /* Name of shop */
    $printer -> selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
    $printer -> text($nama_instansi."\n");
    $printer -> selectPrintMode();
    $printer -> text($hari." ".$tanggal." ".$jam."\n");
    $printer -> feed();

    /* Title of receipt */
    $printer -> setEmphasis(true);
    $printer -> text("YOUR QUEUE\n");
    $printer -> setEmphasis(false);
    $printer -> feed();
//
    $printer -> setJustification(Printer::JUSTIFY_CENTER);
    $printer -> setTextSize(8, 8);
    $printer -> text($no_antrian."\n");
    $printer -> setTextSize(4, 4);
    $printer -> text($loket."\n");
    $printer -> feed();
    $printer -> cut();
    $printer -> pulse();

    $printer -> close();
}

?>

