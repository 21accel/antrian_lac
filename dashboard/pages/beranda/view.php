<div class="page-content">
	<div style="margin-top:55px;" class="row">
		<div class="col-xs-12">
		  	<div class="row center">
			    <div id="load_antrian" class="col-lg-6 col-md-6 col-xs-6"></div>

			    <div class="col-lg-6 col-md-6 col-xs-6">
			      	<iframe width="100%" height="480" src="https://www.youtube.com/embed/?listtype=playlist&list=PLD6t6ckHsruYoalxbzcjX1TNn4h7ShiRk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
			    </div>
		  	</div>
		</div>
	</div><!-- /.row -->
	
	<hr>

	<div class="row">
		<div class="col-xs-12">
		  	<div class="row center">
			    <div class="col-lg-4 col-md-4 col-xs-12">
			      	<button id="simpan_pidana" class="btn btn-app btn-pink">
				        <div>Number</div>
		        		<div style="font-size:50px;margin-top:-10px" id="load_loket1"></div>
				        <div style="border-bottom:1px solid #fff"></div>
		        		<div>COUNTER 1</div>
			      	</button>
			    </div>

			    <div class="col-lg-4 col-md-4 col-xs-12">
			      	<button id="simpan_perdata" class="btn btn-app btn-warning">
				        <div>Number</div>
		        		<div style="font-size:50px;margin-top:-10px" id="load_loket2"></div>
				        <div style="border-bottom:1px solid #fff"></div>
		        		<div>COUNTER 2</div>
			      	</button>
			    </div>

			    <div class="col-lg-4 col-md-4 col-xs-12">
			      	<button id="simpan_tipikor" class="btn btn-app btn-danger">
			        	<div>Number</div>
		        		<div style="font-size:50px;margin-top:-10px" id="load_loket3"></div>
				        <div style="border-bottom:1px solid #fff"></div>
		        		<div>COUNTER 3</div>
			      	</button>
			    </div>
		  	</div>
		</div>
	</div><!-- /.row -->
</div>

<script type="text/javascript">
$(document).ready(function(){ 
    $('#load_antrian').load('pages/beranda/getAntrian.php');
    $('#load_loket1').load('pages/beranda/getLoket1.php');
    $('#load_loket2').load('pages/beranda/getLoket2.php');
    $('#load_loket3').load('pages/beranda/getLoket3.php');

    setInterval( function () {
    	$('#load_antrian').load('pages/beranda/getAntrian.php');
	    $('#load_loket1').load('pages/beranda/getLoket1.php');
	    $('#load_loket2').load('pages/beranda/getLoket2.php');
	    $('#load_loket3').load('pages/beranda/getLoket3.php');
       	table.ajax.reload( null, false );
    }, 1000); // refresh setiap 5000 milliseconds
}); 
</script>