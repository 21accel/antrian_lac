<div class="page-content center">
	<div class="page-header">
		<div style="margin-top:10px;margin-bottom:5px" class="row">
			<div class="col-xs-12">
			  	<img src="../petugas/assets/images/logo-instansi/<?php echo $logo; ?>" alt="Logo" height="100">
			  	<h1 style="color:#fff;font-size:30px"><strong><?php echo strtoupper($nama_instansi); ?></strong></h1>
			</div>
		</div>
	</div>

	<div class="alert alert-purple-light">
	  	<i style="margin-right:5px" class="ace-icon fa fa-info-circle"></i> Welcome to <?php echo $nama_instansi; ?>. Please choose one of these services you want and click print button to get your ticket.
	</div>

	<br>

	<div class="row">
		<div class="col-xs-12">
		  	<div class="row">
			    <div class="col-lg-4 col-md-4 col-xs-12">
                    <button class="btn btn-app btn-purple">
                        <div >Counter 1 - Certificate</div>
                    </button>
                    <button id="simpan_antrian1" class="btn btn-app btn-purple print">
                        <i style="padding:55px 0;font-size:100px" class="ace-icon fa fa-user-plus"></i>
                    </button>

                    <button class="btn btn-app btn-purple">
                        <div id="load_antrian1"></div>
                    </button>
                </div>
			    <div class="col-lg-4 col-md-4 col-xs-12">
                    <button class="btn btn-app btn-purple">
                        <div >Counter 2 - Test</div>
                    </button>
                    <button id="simpan_antrian2" class="btn btn-app btn-purple print">
				        <i style="padding:55px 0;font-size:100px" class="ace-icon fa fa-user-plus"></i> 
			      	</button>

			      	<button class="btn btn-app btn-purple">
		        		<div id="load_antrian2"></div>
			      	</button>
			    </div>
			    <div class="col-lg-4 col-md-4 col-xs-12">
                    <button class="btn btn-app btn-purple">
                        <div >Counter 3 - Course</div>
                    </button>
                    <button id="simpan_antrian3" class="btn btn-app btn-purple print">
                        <i style="padding:55px 0;font-size:100px" class="ace-icon fa fa-user-plus"></i>
                    </button>

                    <button class="btn btn-app btn-purple">
                        <div id="load_antrian3"></div>
                    </button>
                </div>
                <div class="col-lg-4 col-md-4 col-xs-12">
                    <button class="btn btn-app btn-purple print" onclick="location.href='pages/beranda/print.php'">
                        <i style="padding:55px 0;font-size:100px" class="ace-icon fa fa-print"></i>Print your ticket
                    </button>
                </div>
		  	</div>
		</div>
	</div><!-- /.row -->
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#load_antrian1').load('pages/beranda/getAntrian1.php');
        $('#load_antrian2').load('pages/beranda/getAntrian2.php');
        $('#load_antrian3').load('pages/beranda/getAntrian3.php');
        // antrian sertifikat
        $("#simpan_antrian1").on('click',function(){
            $.ajax({
                url   : "pages/beranda/proses1.php",
                type  : "POST",
                cache : false,
                success: function(msg){

                    if(msg=="Sukses"){
                        $('#load_antrian1').load('pages/beranda/getAntrian1.php').fadeIn("slow");
                        $('#load_antrian2').load('pages/beranda/getAntrian2.php').fadeIn("slow");
                        $('#load_antrian3').load('pages/beranda/getAntrian3.php').fadeIn("slow");
                        $.ajax({
                            url : "pages/beranda/print.php",
                            type: "POST",
                            cache: false,
                            //success: function(data, textStatus, jqXHR)
                            success: function()
                            {

                                alert('Please take your ticket');

                            },
                            error:function (){
                                alert("There is an error when printing");
                            }

                        });
                    }

                }

            });
        });

        // antrian Test
        $("#simpan_antrian2").on('click',function(){
            $.ajax({
                url   : "pages/beranda/proses2.php",
                type  : "POST",
                cache : false,
                success: function(msg)
                {
                    if(msg=="Sukses"){
                        $('#load_antrian1').load('pages/beranda/getAntrian1.php').fadeIn("slow");
                        $('#load_antrian2').load('pages/beranda/getAntrian2.php').fadeIn("slow");
                        $('#load_antrian3').load('pages/beranda/getAntrian3.php').fadeIn("slow");
                        $.ajax({
                            url : "pages/beranda/print.php",
                            type: "POST",
                            cache: false,
                            //success: function(data, textStatus, jqXHR)
                            success: function()
                            {

                                alert('Please take your ticket');

                            },
                            error:function (){
                                alert("There is an error when printing");
                            }

                        });

                    }
                }

            });
        });

        // antrian Test
        $("#simpan_antrian3").on('click',function(){
            $.ajax({
                url   : "pages/beranda/proses3.php",
                type  : "POST",
                cache : false,
                success: function(msg)
                {
                    if(msg=="Sukses"){
                        $('#load_antrian1').load('pages/beranda/getAntrian1.php').fadeIn("slow");
                        $('#load_antrian2').load('pages/beranda/getAntrian2.php').fadeIn("slow");
                        $('#load_antrian3').load('pages/beranda/getAntrian3.php').fadeIn("slow");
                        $.ajax({
                            url : "pages/beranda/print.php",
                            type: "POST",
                            cache: false,
                            //success: function(data, textStatus, jqXHR)
                            success: function()
                            {

                                alert('Please take your ticket');

                            },
                            error:function (){
                                alert("There is an error when printing");
                            }

                        });
                    }
                }
            });
        });
    });
</script>
