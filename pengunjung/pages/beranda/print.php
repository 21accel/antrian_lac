<?php
//date_default_timezone_set("ASIA/JAKARTA");

// panggil file config.php untuk koneksi ke database
require_once "../../../config/config.php";
// panggil file fungsi nama hari
require_once "../../../config/fungsi_nama_hari.php";
// panggil library buat printer
require '../../vendor/autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;


$hari_ini = date("Y-m-d");

// fungsi query untuk menampilkan data dari tabel antrian
$result = $mysqli->query("SELECT max(ID) as id FROM antrian") or die('Ada kesalahan pada query tampil nomor antrian: '.$mysqli->error);
$data_id = $result->fetch_assoc();
$id = $data_id['id'];
$result = $mysqli->query("SELECT no_antrian as nomor, loket as counter FROM antrian WHERE ID='$id'") or die('Ada kesalahan pada query tampil nomor antrian: '.$mysqli->error);
$data = $result->fetch_assoc();

$nama_instansi  = $data_config['nama_instansi'];
$loket          = $data['counter'];
$no_antrian     = $data['nomor'];
$hari           = date("l");
$tanggal        = date("d-m-Y");
$jam            = date("H:i:s");

// change this so they can connect
$connector = new WindowsPrintConnector("POS-58");
$printer = new Printer($connector);

$printer->setJustification(Printer::JUSTIFY_CENTER);

/* Name of shop */
$printer -> setTextSize(1, 1);
$printer -> text("Language Center\nTelkom University\n");
$printer -> selectPrintMode();
$printer -> text($hari." ".$tanggal." ".$jam."\n");
$printer -> feed();

/* Title of receipt */
$printer -> setEmphasis(true);
$printer -> text("YOUR QUEUE\n");
$printer -> setEmphasis(false);
$printer -> feed();
//
$printer -> setJustification(Printer::JUSTIFY_CENTER);
$printer -> setTextSize(8, 8);
$printer -> text($no_antrian."\n");
$printer -> setTextSize(4, 4);
$printer -> text($loket."\n");
$printer -> feed();
$printer -> cut();
$printer -> pulse();

$printer -> close();
    echo 'sukses';


?>