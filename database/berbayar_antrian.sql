-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Mar 28, 2019 at 03:59 AM
-- Server version: 5.7.21
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `berbayar_antrian`
--

-- --------------------------------------------------------

--
-- Table structure for table `antrian`
--

DROP TABLE IF EXISTS `antrian`;
CREATE TABLE IF NOT EXISTS `antrian` (
  `ID` bigint(20) NOT NULL,
  `tanggal` date NOT NULL,
  `loket` varchar(15) DEFAULT NULL,
  `no_antrian` char(3) NOT NULL,
  `panggil` char(5) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '0',
  `updated_user` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `antrian`
--

INSERT INTO `antrian` (`ID`, `tanggal`, `loket`, `no_antrian`, `panggil`, `status`, `updated_user`, `updated_date`) VALUES
(1, '2019-03-27', 'Counter 1', '001', '0 0 1', '1', 2, '2019-03-27 16:44:32'),
(2, '2019-03-27', 'Counter 3', '002', '0 0 2', '0', NULL, NULL),
(3, '2019-03-27', 'Counter 2', '003', '0 0 3', '0', NULL, NULL),
(4, '2019-03-27', 'Counter 1', '004', '0 0 4', '0', NULL, NULL),
(5, '2019-03-27', 'Counter 2', '005', '0 0 5', '0', NULL, NULL),
(6, '2019-03-27', 'Counter 3', '006', '0 0 6', '0', NULL, NULL),
(7, '2019-03-27', 'Counter 1', '007', '0 0 7', '0', NULL, NULL),
(8, '2019-03-27', 'Counter 2', '008', '0 0 8', '0', NULL, NULL),
(9, '2019-03-27', 'Counter 1', '009', '0 0 9', '0', NULL, NULL),
(10, '2019-03-27', 'Counter 2', '010', '0 1 0', '0', NULL, NULL),
(11, '2019-03-27', 'Counter 3', '011', '0 1 1', '0', NULL, NULL),
(12, '2019-03-27', 'Counter 1', '012', '0 1 2', '0', NULL, NULL),
(13, '2019-03-27', 'Counter 1', '013', '0 1 3', '0', NULL, NULL),
(14, '2019-03-27', 'Counter 2', '014', '0 1 4', '0', NULL, NULL),
(15, '2019-03-27', 'Counter 1', '015', '0 1 5', '1', 2, '2019-03-27 17:03:53'),
(16, '2019-03-28', 'Counter 1', '001', '0 0 1', '1', 2, '2019-03-28 09:30:55'),
(17, '2019-03-28', 'Counter 1', '002', '0 0 2', '1', 2, '2019-03-28 10:55:40'),
(18, '2019-03-28', 'Counter 1', '003', '0 0 3', '0', NULL, NULL),
(19, '2019-03-28', 'Counter 2', '004', '0 0 4', '0', NULL, NULL),
(20, '2019-03-28', 'Counter 3', '005', '0 0 5', '0', NULL, NULL),
(21, '2019-03-28', 'Counter 2', '006', '0 0 6', '0', NULL, NULL),
(22, '2019-03-28', 'Counter 1', '007', '0 0 7', '0', NULL, NULL),
(23, '2019-03-28', 'Counter 3', '008', '0 0 8', '0', NULL, NULL),
(24, '2019-03-28', 'Counter 1', '009', '0 0 9', '0', NULL, NULL),
(25, '2019-03-28', 'Counter 1', '010', '0 1 0', '0', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sys_config`
--

DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE IF NOT EXISTS `sys_config` (
  `configID` tinyint(1) NOT NULL,
  `nama_instansi` varchar(100) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `telepon` varchar(13) NOT NULL,
  `email` varchar(50) NOT NULL,
  `website` varchar(50) NOT NULL,
  `logo` varchar(50) NOT NULL,
  `updated_user` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`configID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sys_config`
--

INSERT INTO `sys_config` (`configID`, `nama_instansi`, `alamat`, `telepon`, `email`, `website`, `logo`, `updated_user`, `updated_date`) VALUES
(1, 'Language Center Telkom University', 'Kompleks Ruko Bandung Town Square\r\nJl. Gajah Mada No. 187 Kavling A17-18 Bandung', '', 'languagecenter@telkomuniversity.ac.id', 'lac.telkomuniversity.ac.id', 'logo-small.png', 1, '2019-02-28 23:57:44');

-- --------------------------------------------------------

--
-- Table structure for table `sys_database`
--

DROP TABLE IF EXISTS `sys_database`;
CREATE TABLE IF NOT EXISTS `sys_database` (
  `dbID` int(11) NOT NULL,
  `file_name` varchar(50) NOT NULL,
  `file_size` varchar(10) NOT NULL,
  `created_user` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`dbID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sys_database`
--

INSERT INTO `sys_database` (`dbID`, `file_name`, `file_size`, `created_user`, `created_date`) VALUES
(1, '20180618_122846_database.sql.gz', '2 KB', 1, '2018-06-18 05:28:46');

-- --------------------------------------------------------

--
-- Table structure for table `sys_users`
--

DROP TABLE IF EXISTS `sys_users`;
CREATE TABLE IF NOT EXISTS `sys_users` (
  `userID` int(11) NOT NULL,
  `fullname` varchar(30) NOT NULL,
  `user_account_name` varchar(30) NOT NULL,
  `user_account_password` varchar(45) NOT NULL,
  `user_permissions` enum('Administrator','Counter 1','Counter 2','Counter 3') NOT NULL,
  `block_users` enum('Ya','Tidak') NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_user` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `sys_users`
--

INSERT INTO `sys_users` (`userID`, `fullname`, `user_account_name`, `user_account_password`, `user_permissions`, `block_users`, `last_login`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
(1, 'Ibnu Agiel\'s Althur, A.Md', 'admin', '90b9aa7e25f80cf4f64e990b78a9fc5ebd6cecad', 'Administrator', 'Tidak', '2019-03-01 00:10:19', 1, '2018-06-12 17:00:00', 1, '2019-03-01 00:10:19'),
(2, 'Anggie Dwiki Annisa, A.Md.Kom', 'counter1', '663727ee751e51b571184d34602674bd4b064beb', 'Counter 1', 'Tidak', '2019-03-28 09:25:17', 1, '2018-06-15 16:22:54', 2, '2019-03-28 09:25:17'),
(3, 'Jajang Abdul Fattah, S.ST.', 'counter2', 'e0748e097924471fcad9f5056967f07c5f24c9bc', 'Counter 2', 'Tidak', '2019-03-01 00:14:02', 1, '2018-06-15 16:23:26', 3, '2019-03-01 00:14:02'),
(4, 'Counter 3', 'counter3', 'f6568553ac13f53110e224dcfd8b20e351ac178e', 'Counter 3', 'Tidak', '2019-03-01 00:13:31', 1, '2018-06-18 05:19:52', 4, '2019-03-01 00:13:31');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
